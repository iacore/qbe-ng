// forked from https://github.com/flowtr/qbe/blob/main/build.zig

const std = @import("std");

const SRC_COMM = .{ "main.c", "util.c", "parse.c", "abi.c", "cfg.c", "mem.c", "ssa.c", "alias.c", "load.c", "copy.c", "fold.c", "simpl.c", "live.c", "spill.c", "rega.c", "emit.c" };
const SRC_AMD64 = .{ "amd64/targ.c", "amd64/sysv.c", "amd64/isel.c", "amd64/emit.c" };
const SRC_ARM64 = .{ "arm64/targ.c", "arm64/abi.c", "arm64/isel.c", "arm64/emit.c" };
const SRC_RV64 = .{ "rv64/targ.c", "rv64/abi.c", "rv64/isel.c", "rv64/emit.c" };
const SRC = SRC_COMM ++ SRC_AMD64 ++ SRC_ARM64 ++ SRC_RV64;

const QbeTarget = enum {
    arm64_apple,
    amd64_apple,
    arm64,
    rv64,
    amd64_sysv,
};

fn determine_qbedeftgt(a: std.mem.Allocator, target: std.zig.CrossTarget) QbeTarget {
    const cpuarch = target.getCpuArch();
    if (target.isDarwin()) {
        if (cpuarch == .x86_64) return .amd64_apple;
        if (cpuarch == .aarch64) return .arm64_apple;
    }
    if (cpuarch == .x86_64) return .amd64_sysv;
    if (cpuarch == .aarch64) return .arm64;
    if (cpuarch == .riscv64) return .rv64;
    std.log.warn("host target ({s}) not valid output target, default to amd64_sysv", .{target.zigTriple(a) catch unreachable});
    return .amd64_sysv;
}

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const qbe_default_target: QbeTarget = b.option(QbeTarget, "qbedeftgt", "QBE default output target") orelse determine_qbedeftgt(b.allocator, target);

    const exe = b.addExecutable(.{
        .name = "qbe",
        .target = target,
        .optimize = optimize,
    });
    exe.linkLibC();
    exe.addIncludePath(".");
    exe.defineCMacro("Deftgt", std.fmt.allocPrint(b.allocator, "T_{s}", .{@tagName(qbe_default_target)}) catch unreachable);
    exe.addCSourceFiles(
        &SRC,
        &.{
            "-std=c99",
        },
    );
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
